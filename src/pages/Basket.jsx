import List from "../components/List/List";
import { useSelector } from "react-redux";

function Basket() {
  const basketProducts = useSelector((state) => state.products.basket);

  let pageType = "presentIcon";
  return (
    <>
      <List data={basketProducts} pageType={pageType} />
    </>
  );
}

export default Basket;
