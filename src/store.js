import { configureStore } from "@reduxjs/toolkit";
import productReducer from "./productsSlice"; // Імпортуйте ваш ред'юсер правильно
import modalReducer from "./modalSlice";

// Налаштування store
const store = configureStore({
  reducer: {
    products: productReducer,
    modal: modalReducer,
  },
});

export default store;
