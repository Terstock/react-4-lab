import Card from "../Card/Card";
import "./List.scss";

function List({ data, pageType }) {
  return (
    <ul className="list">
      {data.map(({ id, ...item }) => (
        <Card card={{ ...item, id }} key={id} pageType={pageType} />
      ))}
    </ul>
  );
}

export default List;
