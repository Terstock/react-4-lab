import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchProducts = createAsyncThunk(
  "products/fetchProducts",
  async () => {
    const response = await fetch(`./products.json`);
    const data = await response.json();
    console.log(data);
    return data;
  }
);

const productSlice = createSlice({
  name: "products",
  initialState: {
    products: [],
    basket: [],
    favorite: [],
    error: null,
  },
  reducers: {
    setError: (state, action) => {
      state.error = action.payload;
    },
    productReducer: (state, action) => {
      state.products = action.payload;
    },
    basketReducer: (state, action) => {
      state.basket = action.payload;
    },
    favoriteReducer: (state, action) => {
      state.favorite = action.payload;
    },
    addProduct: (state, action) => {
      const isInBasket = state.basket.some(
        (product) => product.id === action.payload.id
      );
      if (!isInBasket) {
        state.basket.push(action.payload);
      }
    },
    deleteFromBasket: (state, action) => {
      state.basket = state.basket.filter(
        (item) => item.id !== action.payload.id
      );
    },
    handleFavorite: (state, action) => {
      const isFavoritesAlready = state.favorite.some(
        (product) => product.id === action.payload.id
      );
      if (isFavoritesAlready) {
        state.favorite = state.favorite.filter(
          (item) => item.id !== action.payload.id
        );
      } else {
        state.favorite = [...state.favorite, action.payload];
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.pending, (state) => {
        state.status = "loading";
      })
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.status = "succeeded";
        state.products = action.payload;
      })
      .addCase(fetchProducts.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export const {
  productReducer,
  addProduct,
  deleteFromBasket,
  basketReducer,
  favoriteReducer,
  handleFavorite,
  setError,
} = productSlice.actions;

export default productSlice.reducer;
